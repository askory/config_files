#! /bin/bash

files="screenrc vimrc tmux.conf"
for f in $files; do
  ln -sf $(pwd)/$f ~/.$f
done
