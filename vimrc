set foldmethod=indent
set autoindent
set tabstop=4
set shiftwidth=4
set expandtab
set hlsearch
set colorcolumn=101                                                                                  
syntax on

hi ColorColumn ctermbg=darkgrey guibg=darkgrey

" The paths make sense when opening vim from within the directory where tree I most often open vim.
let g:JavaImpPaths = getcwd() . "/../../../../prebuilts/sdk/current," . getcwd() . "/src," . getcwd() . "/tests/src"

xnoremap p pgvy

let g:ctrlp_map = '<c-o>'
